const express = require('express');
const router = express.Router();
const moment = require('moment')

const pool = require('../database');
const { isLoggedIn } = require('../lib/auth');

router.get('/', isLoggedIn, async (req, res) => {
    const schedules = await pool.query("SELECT sh.id, sh.day_week, sh.time_start, sh.time_end, sh.classroom, u.fullname, s.name FROM schedules sh inner join users u on sh.user_id = u.id inner join subjects s on sh.subject_id = s.id");
    console.log(schedules);
    res.render('schedules/list', { schedules });
});


router.get('/add', isLoggedIn, (req, res) => {
    res.render('schedules/add');
});

router.post('/add', isLoggedIn, async (req, res) => {
    const { day_week, time_start_input, time_end_input, classroom, user_id, subject_id } = req.body;
    let time_start = moment(time_start_input, "hh:mm").format('h:mm:ss');
    let time_end = moment(time_end_input, "hh:mm").format('h:mm:ss');
    const newObject = {
        day_week,
        time_start,
        time_end,
        classroom,
        user_id,
        subject_id
    };
    await pool.query('INSERT INTO schedules set ?', [newObject]);
    req.flash('success', 'Horario guardado!');
    res.redirect('/schedules');
});


router.get('/delete/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    await pool.query('DELETE FROM schedules WHERE id = ?', [id]);
    req.flash('success', 'User Removed successfully');
    res.redirect('/schedules');
});

router.get('/edit/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const schedules = await pool.query('SELECT * FROM schedules WHERE id = ?', [id]);
    res.render('schedules/edit', { schedules: schedules[0] });
});

router.post('/edit/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const { day_week, time_start_input, time_end_input, classroom, user_id, subject_id } = req.body;
    let time_start = moment(time_start_input, "hh:mm").format('h:mm:ss');
    let time_end = moment(time_end_input, "hh:mm").format('h:mm:ss');
    const newObject = {
        day_week,
        time_start,
        time_end,
        classroom,
        user_id,
        subject_id
    };
    await pool.query('UPDATE schedules set ? WHERE id = ?', [newObject, id]);
    req.flash('success', 'Schedules Updated successfully');
    res.redirect('/schedules');
});


module.exports = router;