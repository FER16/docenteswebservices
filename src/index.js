const express = require('express');
const app = express();
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const path = require('path');
const flash = require('connect-flash');
const session = require('express-session');
const MySQLStore = require('express-mysql-session');
const passport = require('passport');
const { database } = require('./key');

const pool = require('./database');

require('./lib/passport');

//Settings
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    helpers: require('./lib/handlebars')
}));
app.set('view engine', '.hbs');

//middlewares
app.use(session({
    secret: 'docentenodesession',
    resave: false,
    saveUninitialized: false,
    store: new MySQLStore(database)
}));
app.use(flash());
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(passport.initialize());
app.use(passport.session());

//Global Variables
app.use((req, res, next) => {
    app.locals.success = req.flash('success');
    app.locals.message = req.flash('message');
    app.locals.user = req.user;
    next();
});

//Routes
app.use(require('./routes/index'));
app.use(require('./routes/authentication'));
app.use('/users', require('./routes/users'));
app.use('/subjects', require('./routes/subjects'));
app.use('/schedules', require('./routes/schedules'));
app.use('/daily_records', require('./routes/daily_records'));
app.use('/api', require('./routes/api'));

//Public
app.use(express.static(path.join(__dirname, 'public')));


//Starting Server
let port = app.get('port');
const server = app.listen(port, () => {
    console.log(`Server on port ${port}`);
});

//WebSockets
const SocketIO = require('socket.io');
const io = SocketIO(server);
const axios = require("axios");
var qs = require("qs");

io.on('connection', (socket) => {
    socket.on('connection:location', (data) => {
        var json = JSON.parse(data);
        getSchedule(json.usuario, json.Latitude, json.Longitude);
    });
});


async function getSchedule(id, lat, long) {
    const config = { headers: { "content-type": "application/x-www-form-urlencoded" } };
    var bodyParameters = "identification=" + id
    var datos = "";
    await axios.post('http://192.168.1.101:4000/api/schedules', bodyParameters, config)
        .then(function (response) {
            datos = response;

            setTimeout(function () {
                getPlace(lat, long, response.data)
            }, 1000);

        })
        .catch(function (error) {
            //    console.log(error);
        })
        .finally(function () {
        });
    return datos;
}

async function getPlace(latitud, longitud, d) {
    let datos = "";
    await axios.get("https://api.opencagedata.com/geocode/v1/json?q=" + latitud + ",+" + longitud + "&key=4f2e8ad1d18f451793dd42490f6156e1&no_annotations=1&language=es")
        .then(function (response) {
            let jsn = response.data;
            let resultados = jsn.results;
            let objeto = resultados[0];
            let lugar = objeto.formatted;
            let comp = objeto.components;
            let tipoLugar = comp._type;
            //n  console.log(lugar + " " + tipoLugar + " " + d);
        })
        .catch(function (error) {
            //console.log(error);
        })
        .finally(function () {
        });
    return datos;
}