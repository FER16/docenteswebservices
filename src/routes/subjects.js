const express = require('express');
const router = express.Router();
const helpers = require('../lib/helpers');

const pool = require('../database');
const { isLoggedIn } = require('../lib/auth');


router.get('/', isLoggedIn, async (req, res) => {
    const subjects = await pool.query("SELECT * FROM subjects");
    res.render('subjects/list', { subjects });
});

router.get('/add', isLoggedIn, (req, res) => {
    res.render('subjects/add');
});

router.post('/add', isLoggedIn, async (req, res) => {
    const { name, description } = req.body;
    const newobject = {
        name,
        description
    };
    await pool.query('INSERT INTO subjects set ?', [newobject]);
    req.flash('success', 'Asignatura guardada!');
    res.redirect('/subjects');
});


router.get('/delete/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    try {
        await pool.query('DELETE FROM subjects WHERE id = ?', [id]);
        req.flash('success', 'Subject Removed successfully');
        res.redirect('/subjects');
    } catch (e) {
        req.flash('message',e.toString());
        res.redirect('/subjects');
    }

});

router.get('/edit/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const subjects = await pool.query('SELECT * FROM subjects WHERE id = ?', [id]);
    res.render('subjects/edit', { subject: subjects[0] });
});

router.post('/edit/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const { name, description } = req.body;
    if (name.length >= 51) {
        req.flash('message', 'El nombre es demasiado extenso!');
        res.redirect('/subjects/edit/' + id);
    } else {
        const newObject = {
            name,
            description
        };
        await pool.query('UPDATE subjects set ? WHERE id = ?', [newObject, id]);
        req.flash('success', 'Subject Updated successfully');
        res.redirect('/subjects');
    }
});


module.exports = router;