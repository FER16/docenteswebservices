const express = require('express');
const router = express.Router();
const helpers = require('../lib/helpers');

const pool = require('../database');
const { isLoggedIn } = require('../lib/auth');


router.get('/', isLoggedIn, async (req, res) => {
    const daily_records = await pool.query("SELECT dr.created_at, dr.topic, u.identification, u.fullname, sb.name FROM daily_records dr inner join schedules s on dr.schedule_id = s.id inner join users u on s.user_id = u.id inner join subjects sb on s.subject_id = sb.id;");
    res.render('daily_records/list', { daily_records });
});


module.exports = router;