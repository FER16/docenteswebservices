const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const pool = require('../database');
const helpers = require('../lib/helpers');

passport.use('local.signin', new LocalStrategy({
    usernameField: 'identification',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, identification, password, done) => {
    const rows = await pool.query('SELECT * FROM users WHERE identification = ?', [identification]);
    if (rows.length > 0) {
        const user = rows[0];
        const validPassword = await helpers.matchPassword(password, user.password);
        if (validPassword) {
            if (user.rol == 2) {
                done(null, false, req.flash('message', 'No tiene acceso permitido a este lugar'));
            } else {
                done(null, user, null);
            }
        } else {
            done(null, false, req.flash('message', 'Incorrect Password'));
        }
    } else {
        return done(null, false, req.flash('message', 'The Username does not exists'));
    }
}));

passport.use('local.signup', new LocalStrategy({
    usernameField: 'identification',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, identification, password, done) => {
    const { fullname, password2 } = req.body;
    if (password == password2) {
        const newUser = {
            identification,
            rol: 2,
            fullname
        };
        newUser.password = await helpers.encryptPassword(password);
        const result = await pool.query('INSERT INTO users SET ?', [newUser]);
        newUser.id = result.insertId;
        return done(null, newUser);
    } else {
        done(null, false, req.flash('message', 'Las contraseñas deben ser iguales'));
    }
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    const rows = await pool.query('SELECT * FROM users WHERE id = ?', [id]);
    done(null, rows[0]);
});