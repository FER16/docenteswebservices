const express = require('express');
const router = express.Router();
const helpers = require('../lib/helpers');

const pool = require('../database');
const { isLoggedIn } = require('../lib/auth');


router.get('/add', isLoggedIn, (req, res) => {
    res.render('users/add');
});

router.post('/add', isLoggedIn, async (req, res) => {
    const { fullname, identification, rol, password } = req.body;
    const newUser = {
        fullname,
        identification,
        rol
    };
    newUser.password = await helpers.encryptPassword(password);
    await pool.query('INSERT INTO users set ?', [newUser]);
    req.flash('success', 'Usuario guardado!');
    res.redirect('/users');
});

router.get('/', isLoggedIn, async (req, res) => {
    const users = await pool.query("SELECT IF (u.rol=2,'Docente','Administrador') as 'rol_name', u.identification, u.fullname, u.rol from users u");
    res.render('users/list', { users });
});

router.get('/delete/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    await pool.query('DELETE FROM links WHERE id = ?', [id]);
    req.flash('success', 'Link Removed successfully');
    res.redirect('/users');
});

router.get('/edit/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const links = await pool.query('SELECT * FROM users WHERE id = ?', [id]);
    res.render('users/edit', { link: links[0] });
});

router.post('/edit/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const { title, description, url } = req.body;
    const newLink = {
        title,
        description,
        url
    };
    await pool.query('UPDATE links set ? WHERE id = ?', [newLink, id]);
    req.flash('success', 'Link Updated successfully');
    res.redirect('/users');
});


module.exports = router;