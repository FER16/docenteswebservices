const express = require('express');
const router = express.Router();
const helpers = require('../lib/helpers');

const pool = require('../database');

router.post('/signin', async (req, res) => {
    const { identification, password } = req.body;
    const rows = await pool.query('SELECT * FROM users WHERE identification = ?', [identification]);
    if (rows.length > 0) {
        const user = rows[0];
        const validPassword = await helpers.matchPassword(password, user.password);
        if (validPassword) {
            newObject = {
                id: user.id,
                fullname: user.fullname,
                rol: user.rol
            }
            res.status(200).json({ flag: true, message: 'Autorizado', response: newObject });
        } else {
            res.status(200).json({ flag: false, message: 'Credenciales erroneas', response: '' });
        }
    } else {
        res.status(200).json({ flag: false, message: 'Usuario no existe.', response: '' });
    }
});

router.post('/schedules', async (req, res) => {
    const { identification } = req.body;
    const rows = await pool.query('SELECT sh.*, sb.name AS subject, sb.description FROM schedules sh INNER JOIN subjects sb ON sh.subject_id = sb.id  WHERE user_id  = ?', [identification]);
    if (rows.length > 0) {
        const schedules = rows;
        
        res.status(200).json({ flag: true, message: 'Información encontrada.', response: schedules });
    } else {
        res.status(200).json({ flag: false, message: 'El usuario no tiene una programación asignada.', response: '' });
    }
});

router.post('/daily_record', async (req, res) => {
    const { topic, schedule_id } = req.body;
    try {
        const query = `CALL insertDaily_record(?, ?);`;
        const rows = await pool.query(query, [topic, schedule_id]);
        const obj = { "affectedrows: ": rows.affectedRows }
        res.status(200).json({ flag: true, message: 'Información registrada', response: obj });
    } catch (ex) {
        res.status(200).json({ flag: false, message: ex, response: "" });
    }
});
router.post('/get_daily_records', async (req, res) => {
    const { id } = req.body;
    const rows = await pool.query('SELECT dr.created_at, dr.topic, u.identification, u.fullname, sb.name FROM db_docentes.daily_records dr inner join schedules s on dr.schedule_id = s.id inner join users u on s.user_id = u.id inner join subjects sb on s.subject_id = sb.id WHERE u.id = ?;', [id]);
    if (rows.length > 0) {
        const records = rows;
        res.status(200).json({ flag: true, message: 'Información encontrada.', response: records });
    } else {
        res.status(200).json({ flag: false, message: 'El usuario no tiene registros.', response: '' });
    }
});

module.exports = router;